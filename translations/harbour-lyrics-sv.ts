<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="76"/>
        <source>Clear</source>
        <translation>Rensa</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="70"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="180"/>
        <source>Powered by %1</source>
        <translation>Drivs av %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="120"/>
        <source>Artist</source>
        <translation>Artist</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="89"/>
        <source>Copy to clipboard</source>
        <translation>Kopiera till urklipp</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="132"/>
        <source>Song</source>
        <translation>Låt</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="149"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="39"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>Provider</source>
        <translation>Leverantör</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="66"/>
        <source>Enable Media Player scanner</source>
        <translation>Aktivera skanner för mediaspelaren</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="67"/>
        <source>Checks Media Player to get song info. Do not focus any field to allow text substitution.</source>
        <translation>Söker i mediaspelaren efter låtinfo. Fokusera inte något fält, för att tillåta textersättning.</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="76"/>
        <source>Clear cache</source>
        <translation>Rensa cache</translation>
    </message>
</context>
</TS>
